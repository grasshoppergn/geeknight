(ns geeknight.handler
  (:require [compojure.core :refer [defroutes routes]]
            [compojure.route :as route]
            [noir.util.middleware :as noir-middle]
            [geeknight.routes.home :refer [home-routes]]
            [compojure.handler :as handler]
            [hiccup.middleware :refer [wrap-base-url]]))

(defn init []
  (println "geeknight is starting"))

(defn destroy []
  (println "geeknight is shutting down"))

(defroutes app-routes
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (-> (routes home-routes app-routes)
      (handler/site)
      (wrap-base-url)))



