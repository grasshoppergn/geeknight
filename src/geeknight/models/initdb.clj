(ns geeknight.models.initdb
  (:require [clojure.java.jdbc :as sql])
  (:require [geeknight.models.db :as db]))

(defn create-table-users []
  (do
    (try (sql/drop-table :users) (catch Exception e))
    (sql/create-table
     :users
     [:id "INT NOT NULL AUTO_INCREMENT PRIMARY KEY"]
     [:name "VARCHAR(100)"]
     [:email "VARCHAR(100)"]
     [:password "CHAR(64)"])))

(defn create-table-locations []
  (do
    (try (sql/drop-table :locations) (catch Exception e))
    (sql/create-table
     :locations
     [:id "INT NOT NULL AUTO_INCREMENT PRIMARY KEY"]
     [:name "VARCHAR(100)"]
     [:path "VARCHAR(100)"]
     [:description "TEXT"]
     [:size "INT"]
     [:longtitude "VARCHAR(20)"]
     [:latitude "VARCHAR(20)"])))

(defn create-table-projects []
  (do
    (try (sql/drop-table :projects) (catch Exception e))
    (sql/create-table
     :projects
     [:id "INT NOT NULL AUTO_INCREMENT PRIMARY KEY"]
     [:name "VARCHAR(100)"]
     [:description "TEXT"]
     [:status "VARCHAR(100)"]
     [:user_id "INT"])))

(defn create-table-loc-to-proj []
  (do
    (try (sql/drop-table :loc_to_proj) (catch Exception e))
    (sql/create-table
     :loc_to_proj
     [:location_id "INT"]
     [:project_id "INT"])))

(defn create-table-budget []
  (do
    (try (sql/drop-table :budgets) (catch Exception e))
    (sql/create-table
     :budgets
     [:id "INT NOT NULL AUTO_INCREMENT PRIMARY KEY"]
     [:name "VARCHAR(100)"]
     [:description "TEXT"]
     [:volume "INT"]
     [:project_id "INT"])))

(defn create-table-project-comments []
  (do
    (try (sql/drop-table :project_comments) (catch Exception e))
    (sql/create-table
     :project_comments
     [:id "INT NOT NULL AUTO_INCREMENT PRIMARY KEY"]
     [:project_id "INT"]
     [:user_id "INT"]
     [:content "TEXT"]
     [:vote "TINYINT"]
     [:timestamp "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"])))

(defn create-table-budget-comments []
  (do
    (try (sql/drop-table :budget_comments) (catch Exception e))
    (sql/create-table
     :budget_comments
     [:id "INT NOT NULL AUTO_INCREMENT PRIMARY KEY"]
     [:budget_id "INT"]
     [:user_id "INT"]
     [:content "TEXT"]
     [:vote "TINYINT"]
     [:timestamp "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"])))

(defn create-table-participations []
  (do
    (try (sql/drop-table :participations) (catch Exception e))
    (sql/create-table
     :participations
     [:id "INT NOT NULL AUTO_INCREMENT PRIMARY KEY"]
     [:user_id "INT"]
     [:project_id "INT"]
     [:type "TINYINT"]
     [:volume "INT"]
     [:timestamp "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"])))

(defn fill-tables []
  (do (sql/insert-record :locations
                       {:name "Irkutsk state administration"
                        :path "i/admin.jpg"
                        :description "Make some enchancements to roads"
                        :size 1
                        :longtitude "104.2803"
                        :latitude "52.2899"})
     (sql/insert-record :locations
                       {:name "Banches setup"
                        :description "Cleaning, making the territory better"
                        :path "i/IMG_4084.JPG"
                        :size 400
                        :longtitude "104.2877"
                        :latitude "52.2779"})
     (sql/insert-record :locations
                       {:name "Lenina street 20"
                        :description "Making some enchancements to roads"
                        :path "i/IMG_3418.JPG"
                        :size 400
                        :longtitude "104.2802"
                        :latitude "52.2839"})))

(defn init []
  (sql/with-connection db/db
    (do
      (create-table-users)
      (create-table-locations)
      (create-table-budget)
      (create-table-projects)
      (create-table-loc-to-proj)
      (create-table-participations)
      (create-table-budget-comments)
      (create-table-project-comments)
      (fill-tables))))
;    (sql/with-query-results res ["select host, user from user"]
;      (doall res))))
