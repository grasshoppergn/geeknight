(ns geeknight.models.db
  (:require [clojure.java.jdbc :as sql]))

(def db {:classname "com.mysql.jdbc.Driver"
         :subprotocol "mysql"
         :subname "//localhost:3306/geeknight"
         :user "root"
         :password ""})

(defn getLocations []
  (sql/with-connection db
    (sql/with-query-results res [(str "select * from locations")]
      (doall res))))

(defn getProjects []
  (sql/with-connection db
    (sql/with-query-results res [(str "select * from projects")]
      (doall res))))

(defn addLocation [name desc size lon lat]
  (sql/with-connection db
    (sql/insert-record :locations {:name name
                                   :description desc
                                   :size size
                                   :longtitude lon
                                   :latitude lat})))
