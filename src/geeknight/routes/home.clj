(ns geeknight.routes.home
  (:require [compojure.core :refer :all]
            [liberator.core :refer [defresource resource request-method-in]]
            [geeknight.models.initdb :as initdb]
            [geeknight.models.db :as db]
            [noir.io :as io]
            [noir.validation :as vali]
            [cheshire.core :refer [generate-string]]
            [clojure.java.io :refer [file input-stream]]))

(defresource home
  :available-media-types ["text/html"]
  :exists?
  (fn [context]
    [(io/get-resource "/home.html")
     {::file (file (str (io/resource-path) "/home.html"))}])
  :handle-ok
  (fn [{{{resource :resource} :route-params} :request}]
    (input-stream (io/get-resource "/home.html")))
  :last-modified
  (fn [{{{resource :resource} :route-params} :request}]
    (.lastModified (file (str (io/resource-path) "/home.html")))))

(defresource location_get_list
  :allowed-methods [:get]
  :handle-ok
  (fn [context]
    (generate-string (db/getLocations)))
  :available-media-types ["application/json"])

(defresource project_get_list
  :allowed-methods [:get]
  :handle-ok
  (fn [context]
    (generate-string (db/getProjects)))
  :available-media-types ["application/json"])

(defn valid? [name size lon lat]
  (cond
   (nil? name) "Name is required"
   (nil? size) "Size is required"
   (nil? lon) "Longtitude required"
   (nil? lat) "Latitude required"))

(defn location_add [request]
  (let [param (request :form-params)
        name (param "name")
        desc (param "desc")
        size (param "size")
        lon  (param "lon")
        lat  (param "lat")]
    (prn param)
   (if-let [errors (valid? name size lon lat)]
     (do (prn "----EROO" errors)
     {:status 200 :headers {} :body (generate-string {:error errors})})
    (do (db/addLocation name desc size lon lat)
      {:status 200 :headers {} :body (generate-string {:status "ok"})}))))

(defroutes home-routes
  (GET "/db" [] (initdb/init))
  (ANY "/location/get_list" request location_get_list)
  (ANY "/location/add" [:as r] (location_add r))
  (ANY "/projects/get_list" request project_get_list)
  (ANY "/" request home))

