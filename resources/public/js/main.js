/**
 * @jsx React.DOM
 */
var data = [
    {name: "Charts",
     children: [
         {name: "Cartesian",
          children: [
              {name: "Area", children: null},
              {name: "Bar", children: null},
              {name: "Column", children: null},
              {name: "Finance", children: null},
              {name: "Line", children: null},
              {name: "Skatter", children: null}
          ]},
          {name: "Pie", children: null}
    ]},
    {name: "Gantt", children: null}
];


function setData(items) {

    var ChartItem = React.createClass({
        render: function() {
            if (this.props.data != undefined){
                var available_items = this.props.data.map(function (item) {
                    return (
                        <li>
                        {item.name}
                        <ChartItem data={item.children} />
                        </li>
                        );
                });

                return (
                    <ul className="available_items">
                    {available_items}
                    </ul>
                );
            }
            return (<ul className="no_items"></ul>);
        }
    });


    var ItemBox = React.createClass({
        render: function() {
            return (
                <ChartItem data={this.props.data} />
                );
        }
    });

    React.renderComponent(
        <ItemBox data={items}/>,
        document.getElementById('item-list')
    );
}

var map, myLatlng;
function initialize_map() {
    myLatlng = new google.maps.LatLng(52.2855, 104.2889);
    var myOptions = {
        zoom: 15,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map"), myOptions);


    google.maps.event.addListener(map, "click", function (event) {
        var latitude = event.latLng.lat();
        var longitude = event.latLng.lng();
        console.log( latitude + ', ' + longitude );
        $('#lon').val(longitude);
        $('#lat').val(latitude);
        $('#myModal h3').html('Новая локация ' + latitude + ', ' + longitude);
        $('#myModal').modal();
    });
}
function setMarkers(items){
    for (var i in items){
        myLatlng = new google.maps.LatLng(items[i]['latitude'], items[i]['longtitude']);

        marker = $('<div id = "' + items[i]['id'] + '" class="location "</div>');
        marker.append($('<h2>' + items[i]['name'] +'</h2>'))
        marker.append($('<p>' + items[i]['description'] +'</p>'))
        marker.append($('<img src="' + items[i]['path'] + '">'))
        console.log(marker);
        $('#preview_location_map').append(marker);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title:items[i]['name'],
            id:items[i]['id']
        });
        google.maps.event.addListener(marker, 'click', function() {
            $('.location').hide();
            console.log('.location#' + this.id);
            $('#' + this.id).show();
        });

    }
}

$(function(){
//    setData(data);
    $.get("/location/get_list", setMarkers);
    $('.display').css('min-height', window.innerHeight - 262);
    $('#map').css('height', window.innerHeight - 62);
    initialize_map();

});

